package call.me.bum.sekeletonproject.login;

import call.me.bum.sekeletonproject.base.BasePresenter;
import call.me.bum.sekeletonproject.models.User;

public class LoginActivityPresenter extends BasePresenter<ILogin.view> implements ILogin.presenter{

  private User mUser;

  public LoginActivityPresenter(ILogin.view view) {
    super(view);
    this.mUser = new User();
  }

  public void updateFullName(String fullName) {
    mUser.setFullName(fullName);
    getView().updateUserInfoTextView(mUser.toString());
  }

  public void updateEmail(String email) {
    mUser.setEmail(email);
    getView().updateUserInfoTextView(mUser.toString());
  }

}
