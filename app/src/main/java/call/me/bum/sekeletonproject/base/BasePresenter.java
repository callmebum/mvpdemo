package call.me.bum.sekeletonproject.base;

public abstract class BasePresenter<T extends BaseView>  {

  private T mView;

  public BasePresenter(T view) {
    mView = view;
  }

  public T getView() { return mView; };
}
