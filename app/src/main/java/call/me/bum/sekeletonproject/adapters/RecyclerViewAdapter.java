package call.me.bum.sekeletonproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import call.me.bum.sekeletonproject.R;
import call.me.bum.sekeletonproject.models.Data;
import com.bumptech.glide.Glide;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

  private LayoutInflater mInflater;
  private Context mContext;
  private List<Data> mDatas;
  public RecyclerViewAdapter(Context context) {
    mInflater = LayoutInflater.from(context);
    mContext = context;
  }

  @NonNull
  @Override
  public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = mInflater.inflate(R.layout.recyclerview_item, parent, false);
    return new RecyclerViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
    if (mDatas != null) {
      // load image from data to view
      Glide.with(mContext)
          .load(mDatas.get(position).getImageUrl())
          .into(holder.mImage);
      // set title
      holder.mTitle.setText(mDatas.get(position).getTitle());
    }
  }

  @Override
  public int getItemCount() {
    if (mDatas != null) {
      return mDatas.size();
    } else return 0;
  }

  public void setDatas(ArrayList<Data> datas) {
    mDatas = datas;
    notifyDataSetChanged();
  }

  class RecyclerViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.image)
    CircleImageView mImage;
    @BindView(R.id.title)
    TextView mTitle;

    public RecyclerViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }

}
