package call.me.bum.sekeletonproject.login;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import call.me.bum.sekeletonproject.R;
import call.me.bum.sekeletonproject.adapters.RecyclerViewAdapter;
import call.me.bum.sekeletonproject.base.BaseActivity;

public class LoginActivity extends BaseActivity implements ILogin.view {

  // ui
  TextView mText;
  // vars
  // presenter
  LoginActivityPresenter mPresenter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    //initRecyclerView();
    mPresenter = new LoginActivityPresenter(this);
    mText = findViewById(R.id.myTextView);
    EditText userName = findViewById(R.id.username);
    EditText email = findViewById(R.id.email);
    userName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        mPresenter.updateFullName(charSequence.toString());
      }

      @Override
      public void afterTextChanged(Editable editable) {

      }
    });
    email.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        mPresenter.updateEmail(charSequence.toString());
      }

      @Override
      public void afterTextChanged(Editable editable) {

      }
    });
  }

  @Override
  public void updateUserInfoTextView(String info) {
    mText.setText(info);
  }

  // Recyclerview
  //  private void initRecyclerView() {
//    RecyclerView recyclerView = findViewById(R.id.recycler_view);
//    recyclerView.setAdapter(new RecyclerViewAdapter(this));
//    recyclerView.setLayoutManager(new LinearLayoutManager(this));
//  }
}
