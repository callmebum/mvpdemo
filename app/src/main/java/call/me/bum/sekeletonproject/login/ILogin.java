package call.me.bum.sekeletonproject.login;

import call.me.bum.sekeletonproject.base.BaseView;

public interface ILogin {

  interface view extends BaseView {

    void updateUserInfoTextView(String info);
  }

  interface presenter {
    public void updateFullName(String fullName);
    public void updateEmail(String email);
  }

}
